package com.sample.wireviewer.model

sealed class CharacterData {
    class Success(val data: List<Character>) : CharacterData()
    object Error : CharacterData()
    object NoData: CharacterData()
    object Empty: CharacterData()
}